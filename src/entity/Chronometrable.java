package entity;

import java.io.Serializable;

import entity.ia.BasicIA;

public interface Chronometrable extends Serializable{
	boolean getNoInput();
	void setNoInput();
	float x();
	float y();
	void setIA(BasicIA ia);
	BasicIA getIA();
	
	boolean terminated();
	void finish();
	
	int getTargetGate();
	void increaseTargetGate();
	public void addTime(float f);
	public float getTime(int index);
	public int getNbTime();
	void reinit();
	
	void move();
	
	void forward();
	void backward();
	void turnLeft();
	void turnRight();
	
	double correlation();
	int getNbMove();
	void setNbMoveTillCheckPoint(int nbMove);
	int getNbMoveTillCheckPoint();
	
	void save(String path);
	static Chronometrable load(String path) {return null;}
	float speedY();
	float speedX();
}
