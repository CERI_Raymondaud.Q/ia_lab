package env;

import java.io.Serializable;

import entity.Chronometrable;

public interface ChronoChecker extends Serializable {
	
	float x();
	float y();
	boolean check(Chronometrable c);
	float getTimeOf(Chronometrable c);
	int getNbChecked();
	void start();
	void reinit();
	//float distanceTo(Chronometrable chronometrable);
}
