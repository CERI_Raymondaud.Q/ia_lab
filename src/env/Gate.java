package env;

import java.util.HashMap;

import entity.Chronometrable;

public class Gate implements ChronoChecker {

	
	private static final long serialVersionUID = 1L;
	protected int posX,posY;
	public int nbChecked=0;
	long start=0;
	private HashMap< Chronometrable,Float > times = null;
	
	public Gate( int x, int y) {
		times = new HashMap<Chronometrable,Float>();
		posX = x;
		posY = y;
	}
	
	public void reinit(){
		times = new HashMap<Chronometrable,Float>();
		nbChecked=0;
		start=0;
	}
	
	public float getTimeOf(Chronometrable c) {
		return times.get(c);
	}
	
	public void start(){
		start = System.currentTimeMillis();
	}
	
	public int getNbChecked(){
		return nbChecked;
	}
	
	public boolean check(Chronometrable c){
		if  ( ( posX-c.x() < 10 && posX-c.x() > -10) && ( posY-c.y() < 10 && posY-c.y() > -10) ){
			float millisElapsed = System.currentTimeMillis()-start;
			times.put(c, millisElapsed/1000F);
			c.addTime(millisElapsed/1000F);
			c.setNbMoveTillCheckPoint(c.getNbMove());
			nbChecked+=1;
			return true;
		}
		return false;
	}
	
	public float x(){
		return posX;
	}
	
	public float y(){
		return posY;
	}
}
