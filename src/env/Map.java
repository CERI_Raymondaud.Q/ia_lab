package env;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Vector;
import entity.Chronometrable;
import entity.EntityData;
import entity.ia.GeneticLearning;

public class Map implements Serializable {

	private static final long serialVersionUID = 1L;
	protected int sizeX,sizeY;
	Vector<ChronoChecker> gate;
	Vector<Chronometrable> racers ;
	
	public Map()
	{
		racers= new Vector<Chronometrable>();
		gate = new Vector<ChronoChecker>();
	}
	
	public Map(int x, int y) {
		sizeX = x;
		sizeY = y;
		gate = new Vector<ChronoChecker>();
		racers = new Vector<Chronometrable>();
	}
	
	public ChronoChecker getGate ( int  index)	{
		return gate.get(index);
	}
	
	public int getNbGate()
	{
		return gate.size();
	}
	
	public Chronometrable getVehicle(int index) { // 0 Manual vehicle
		return racers.get(index);
	}
	
	public void restart()
	{
		for ( ChronoChecker c : gate )
			c.reinit();
		for ( Chronometrable cm : racers )
			cm.reinit();
		startGateChrono();
	}
	
	public float getTimeOf(Chronometrable c) {
		return gate.get(gate.size()-1).getTimeOf(c);
	}
	
	
	public void startGateChrono()
	{
		for ( ChronoChecker c : gate )
			c.start();
	}
	
	public void removeAllGates()
	{
		gate = new Vector<ChronoChecker>();
		for ( Chronometrable c : racers )
			c.reinit();
	}
	
	public Vector<EntityData> getRacersPos(){
		Vector<EntityData> racersPos = new Vector<EntityData>();
		for ( Chronometrable c : racers )
			racersPos.add(new EntityData(c.x(),c.y(),0));
		return racersPos;	
	}
	
	public Vector<EntityData> getGatesPos(){
		Vector<EntityData> gatePos = new Vector<EntityData>();
		for ( ChronoChecker c : gate )
			gatePos.add(new EntityData(c.x(),c.y(),c.getNbChecked()));
		return gatePos;	
	}
	
	public void addVehicle(Chronometrable c){
		if (c != null) {
			if (racers==null )
				racers=new Vector<Chronometrable>();
			racers.add(c);
		}
	}
	
	public void addGate(ChronoChecker g, boolean start) {
		gate.add(g);
		if ( start )
			g.start();
	}
	
	public void run() {
		boolean finished = true;
		for ( Chronometrable c : racers ) {
			c.move();
			if ( c.getTargetGate() < gate.size() && gate.get(c.getTargetGate()).check(c) ){
				c.increaseTargetGate();
				//System.out.println("CHECKED FOR VEHICLE" + c );
			}
			if ( ! c.terminated() )
				finished = false;
			//else
				//c.forward();
		}
		if ( finished )
		{
			System.out.println("RESTART");
			if ( racers != null && racers.size() > 1)
				racers = GeneticLearning.mutate(racers);
			else if ( racers == null)
				racers = new Vector<Chronometrable>();
			restart();
		}
	}
	
	public int sizeX(){
		return sizeX;
	}
	
	public int sizeY() {
		return sizeY;
	}
	
	public void save(String fileName)
	{
		try {
			FileOutputStream f = new FileOutputStream(new File(fileName));
			ObjectOutputStream o = new ObjectOutputStream(f);
			o.writeObject(this);
			o.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
	}
	
	public static Map load(String fileName)
	{
		Map lao = null;
		try {
			FileInputStream fi = new FileInputStream(new File(fileName));
			ObjectInputStream oi = new ObjectInputStream(fi);

			// Read objects
			lao = (Map) oi.readObject();
			oi.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lao;
	}
}
