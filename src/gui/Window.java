package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import entity.EntityData;
import entity.Vehicle;
import env.Gate;
import env.Map;

public class Window extends JFrame
{
	private static final long serialVersionUID = 1L;
	JPanel panel ;
	Map map;
	boolean placingGate = true;
	Vehicle v;
	public Window(Map m){
		super("IA LAB");
		map = m;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(m.sizeX(),m.sizeY());
		setVisible(true);
		reinitPanel();
		repaint();
	}
	
	public void reinitPanel(){
		v = (Vehicle) map.getVehicle(0);
		if (panel != null )
			remove(panel);
		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
		    	super.paintComponent(g); // Clear previous
		    	
		    	for ( EntityData e : map.getRacersPos() )
		    		g.fillOval((int)e.x - 5, (int)e.y - 5, 10, 10);
		    	
		    	for ( EntityData e : map.getGatesPos() ){
		    		if ( e.nbChecked > 0 )
		    			g.setColor(Color.BLACK);
		    		else
		    			g.setColor(Color.RED);
		    		g.drawOval((int)e.x - 10, (int)e.y - 10, 20, 20);
		    	}
		    }
		};
		
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			    int x=e.getX();
			    int y=e.getY();
			    System.out.println("Gate placed at" + x+","+y);
			    map.addGate(new Gate(x,y),!placingGate);
			    panel.repaint();
			}
		});
		
		addKeyListener(new KeyListener() {
	        @Override
	        public void keyTyped(KeyEvent e) {
	            // TODO Auto-generated method stub
	        }

	        @Override
	        public void keyReleased(KeyEvent e) {
	            v.setNoInput();
	        }
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_SPACE){
					if ( placingGate ){
						placingGate = false;
						map.startGateChrono();
						panel.repaint();
					}
				}
				/*if (e.getKeyCode() == KeyEvent.VK_UP){
					v.forward();
					panel.repaint();
				}
				if (e.getKeyCode() == KeyEvent.VK_DOWN){
					v.backward();
					panel.repaint();
				}
				if (e.getKeyCode() == KeyEvent.VK_LEFT){
					v.turnLeft();
					panel.repaint();
				}
				if (e.getKeyCode() == KeyEvent.VK_RIGHT){
					v.turnRight();
					panel.repaint();
				}*/
				if (e.getKeyCode() == KeyEvent.VK_ENTER){
					System.out.println("LOAD LAST MAP");
					map = Map.load("current");
					//v = (Vehicle) map.getVehicle(0);
					repaint();
				}
				
				if (e.getKeyCode() == KeyEvent.VK_ALT){
					System.out.println("CLEAN");
					map.removeAllGates();
					panel.repaint();
				}
				
				if (e.getKeyCode() == KeyEvent.VK_CONTROL){
					System.out.println("SAVE CURRENT MAP");
					map.save("current");					
				}
				
			}
		});
		add(panel);
		repaint();
	}
	
	public void run()
	{
		while ( placingGate ) {
			try {
				Thread.sleep(3);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		try {
			Thread.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		map.run();
		panel.repaint();
	}
	
}
